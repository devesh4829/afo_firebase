const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const admin = require('firebase-admin');
const _ = require("lodash");
const { USER_ID_KEY, RAW_DATE_KEY, DATE_KEY, START_DATE_KEY, DATE_FORMAT, DATE_TIME_FORMAT, END_DATE_KEY } = require('../constants');

exports.getUserActivity = async function(req, res, db) {
    console.log("db ", db)
    const userID = req.body.userID || "1"
    let activities = []
    const now = moment().utcOffset("+05:30")
    const startOfMonth = moment().utcOffset("+05:30").startOf('month')
    const startOfWeek = moment().utcOffset("+05:30").startOf('week')
    let startDate = req.body.startDate ? moment(req.body.startDate).startOf('month') : startOfMonth
    if(startDate.isSameOrAfter(startOfMonth)) {
        startDate = startOfMonth;
    }
    if(startOfWeek.isSameOrBefore(startOfMonth)) {
        startDate = startOfWeek;
    }
    let endDate = req.body.endDate ? moment(req.body.endDate).utcOffset("+05:30") : now
    const days = []
    
    let weeklyNumberOfActivities = 0;
    let monthlyNumberOfActivities = 0;
    let weeklyFitScore = 0;
    let monthlyFitScore = 0;
    let weeklyMinutes = 0;
    let monthlyMinutes = 0;        
    let weeklySteps = 0;
    let monthlySteps = 0;
    let weeklyCalories = 0;
    let monthlyCalories = 0;
    
    //const userRef = db.collection('users');
    const activitiesRef = db.collection("activities")
    const calendarRef = db.collection("calendar")
    
    let calendarDetails = []
    let activitiesDb = []
    try{
        //getCalanderData
        //calendarDetails = await calendarRef.where(USER_ID_KEY, "==", userID).where(DATE_KEY,">=",startDate.format(DATE_FORMAT)).orderBy(DATE_KEY, 'desc').get();
        calendarDetails = await calendarRef.where(USER_ID_KEY, "==", userID).orderBy(RAW_DATE_KEY, 'desc').get();

    } catch(error) {
        console.log("Error fetching calendar",error)
    }
    try{
    //getActivitiesData
        console.log("moment(startDate)",startDate)
        console.log("moment(endDate)",endDate)
    
        //activitiesDb = await activitiesRef.where(USER_ID_KEY, "==", userID).where(START_DATE_KEY,">=",startDate.valueOf()).get()
        activitiesDb = await activitiesRef.where(USER_ID_KEY, "==", userID).get()

        activitiesDb.forEach(doc => {
            activities.push(doc.data())
        });
       
    } catch(e){
        console.log("Error fetching Activities",e)
    }

    calendarDetails.forEach(doc => {
        let obj = {}
        let todayActivityArray = []
        
        for (let i=0;i<doc.data().ActivityIDs.length;i++) {
            console.log("ActivityID: ", doc.data().ActivityIDs[i])
            let todayActivityObject = _.find(activities, (o) => o.ActivityID == doc.data().ActivityIDs[i])
           
            let activity = {
                activityName : todayActivityObject.ActivityName,
                activityScore : todayActivityObject.ActivityScore,

                activityStartDate : moment(todayActivityObject.StartDate).utcOffset("+05:30").valueOf(),
                activityEndDate : moment(todayActivityObject.EndDate).utcOffset("+05:30").valueOf(),
                activityUserID : todayActivityObject.userID,
                activityCaloriesBurned : todayActivityObject.CaloriesBurned,
                activityStepCount: todayActivityObject.StepCount,
                activityStepFitPoint: todayActivityObject.StepFitPoints,
                activityDuration: todayActivityObject.Duration,
                activityID : todayActivityObject.ActivityID,
                activityDistance : todayActivityObject.Distance,
                activityAvgPace : todayActivityObject.AveragePace,
                activitySource : todayActivityObject.Source

            }

            let activityStatDate = moment(todayActivityObject.StartDate).utcOffset("+05:30")
            
            //Collate weekly data
            if (startOfWeek.isSameOrBefore(activityStatDate) && now.isSameOrAfter(activityStatDate)) {
                weeklyNumberOfActivities ++

                if(activity.activityScore){
                    weeklyFitScore +=  Number(activity.activityScore) 
                } else {
                    if(activity.activityStepFitPoint){
                        weeklyFitScore +=  Number(activity.activityStepFitPoint)
                    }
                }
                if (activity.activityDuration) {
                    weeklyMinutes +=  Number(activity.activityDuration) 
                }
                if(activity.activityStepCount) {
                    weeklySteps +=  Number(activity.activityStepCount) 
                }
                if(activity.activityCaloriesBurned) {
                    weeklyCalories +=  Math.round(Number(activity.activityCaloriesBurned) * 100) /100
                }
            }

            //Collate monthly data
            if (startOfMonth.isSameOrBefore(activityStatDate) && now.isSameOrAfter(activityStatDate)) {

                console.log("Inside this month ", moment(doc.data().StartDate).utcOffset("+05:30").format(DATE_FORMAT))
                monthlyNumberOfActivities ++
                if(activity.activityScore){
                     monthlyFitScore += Number(activity.activityScore) 
                } else {
                    if(activity.activityStepFitPoint){
                        monthlyFitScore += Number(activity.activityStepFitPoint) 
                    }
                }
                if (activity.activityDuration){
                    monthlyMinutes += Number(todayActivityObject.Duration) 
                }
                if(activity.activityStepCount){
                    monthlySteps +=  Number(activity.activityStepCount)
                }
                if(activity.activityCaloriesBurned){
                    monthlyCalories +=  Math.round( Number(activity.activityCaloriesBurned) * 100) /100
                }
            }
            todayActivityArray.push(activity);
        }

        obj.activityDate = moment(doc.data().Date, DATE_FORMAT).utcOffset("+05:30").valueOf()
        obj.dailyFitScore = doc.data().DailyFitScore
        obj.activitiesForDay = todayActivityArray
        days.push(obj)
    })
      
    let weeklyData = {
        numberOfActivities : weeklyNumberOfActivities,
        fitScore: weeklyFitScore,
        minutes: weeklyMinutes,
        steps: weeklySteps,
        calories: weeklyCalories
    }
    let monthlyData = {
        numberOfActivities : monthlyNumberOfActivities,
        fitScore: monthlyFitScore,
        minutes: monthlyMinutes,
        steps: monthlySteps,
        calories: monthlyCalories
    }
    let callback = {}
    callback = {
        success: true,
        status: 200,
        data: {
            userID: userID,
            Days: days,
            weeklyData: weeklyData,
            monthlyData: monthlyData
        }
    }

    console.log("callback", callback)
    
    return callback
    // Send back a message that we've successfully written the message
    
};
