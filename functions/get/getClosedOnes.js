const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const _ = require("lodash")
const { DATE_TIME_FORMAT, DATE_KEY, USER_ID_KEY, DATE_FORMAT, DAILY_FIT_SCORE_KEY, START_DATE_KEY } = require('../constants');

//const admin = require('firebase-admin');
// admin.initializeApp();
//const db = admin.firestore();

// const getGoogleFitData = require('./getGoogleFitData');
// const getFitBitData = require('./getFitBitData');
// const getGarminData = require('./getGarminData');

exports.getClosedOnes = async function (req, res, db) {
    
    let userIds =[] 
    userIds = req.body.userIds
    console.log("inside get closedOnes data", req.body)
    let response = {};

    if(userIds && typeof(userIds) == "string"){
      console.log("user Ids are comming as string")
      try{
         userIds = JSON.parse(userIds);}
      catch(e){
         userIds = [""]
      }
   }
    if(!userIds) {
      //  response = {
      //     status : 400,
      //     responseMessage : "Undefined Input! please provide valid list of users " + req.body
      //  }
      //  return response
      userIds = [""]
    }
   
    const activitiesRef = db.collection("activities")

    response.status = "Success";
    response.message = "Expected Data Recived";
    let responseBody = [];
    
    const endDate = moment().utcOffset("+05:30");

    // calculate sunday for today's day
    let startDate = moment().utcOffset("+05:30").startOf('week');
    let current = startDate;
    var daysOfWeek = [];

    let allActivities = []

    var i,j,userIdsChunk,chunk = 10;
    for (i=0,j=userIds.length; i<j; i+=chunk) {

    userIdsChunk = userIds.slice(i,i+chunk);

    let  activitiesDb = await activitiesRef.where(USER_ID_KEY, "in", userIdsChunk)
                                           .where(START_DATE_KEY,">=",startDate.valueOf())
                                           .get()
    
    activitiesDb.forEach(doc => { allActivities.push(doc.data()) });
    // do whatever
   }
    
    while (current.isSameOrBefore(endDate, 'day')) {

      let day = {};
      day.date = current.valueOf();
      let closedOnesForDay = [];
      try{
         //Getting data for the closed Ones
       closedOnesForDay =  await getClosedOnesForDay(current, userIds, db, allActivities);
      }
      catch(e){
         response.status = "Failed";
         response.message = e.message;
         return response;
      }
      day.closedOnes = closedOnesForDay;
      daysOfWeek.push(day);
      current.add(1, 'days');
    }

    response.body = daysOfWeek

    return response;
};

var getClosedOnesForDay = async function (currentDay, userIds, db, allActivities) {

let closedOncesForDay = [];
let temp = [];
   //get closedonce from db
const calendarRef = db.collection("calendar")
let i,j,userIdsChunk,chunk = 10;
let usersOnDayDB = [];

for (i=0,j=userIds.length; i<j; i+=chunk) {

   userIdsChunk = userIds.slice(i,i+chunk);
   let usersOnDayDB1 = await calendarRef.where(DATE_KEY, '==' ,currentDay.format(DATE_FORMAT))
                                        .where(USER_ID_KEY, 'in', userIdsChunk)
                                        .orderBy(DAILY_FIT_SCORE_KEY, 'desc')
                                        .get();
   usersOnDayDB1.forEach(doc => { usersOnDayDB.push(doc) });
    // do whatever
}
console.log(currentDay)
usersOnDayDB.forEach(doc => {
  let activitiesIDs = doc.data().ActivityIDs;
   let data = {
       fitScoreForDay : doc.data().DailyFitScore,
       userId:doc.data().UserID,
       activitiesForDayId : activitiesIDs
    }  
   console.log(data)
   temp.push(data);
 
   let activitiesForDay =[]
   for (let i=0 ;i<activitiesIDs.length;i++) {
      let todayActivityObject = _.find(allActivities, (o) => o.ActivityID == activitiesIDs[i])
      let activity = {
         activityName : todayActivityObject.ActivityName,
         activityScore : todayActivityObject.ActivityScore,
         activityStartDate : moment(todayActivityObject.StartDate).valueOf(),
         activityEndDate : moment(todayActivityObject.EndDate).valueOf(),
         activityUserID : todayActivityObject.userID,
         activityCaloriesBurned : todayActivityObject.CaloriesBurned,
         activityStepCount: todayActivityObject.StepCount,
         activityStepFitPoint: todayActivityObject.StepFitPoints,
         activityDuration: todayActivityObject.Duration,
         activityID : todayActivityObject.ActivityID,
         activityDistance : todayActivityObject.Distance,
         activityAvgPace : todayActivityObject.AveragePace,
         activitySource : todayActivityObject.Source
     }
      activitiesForDay.push(activity)
   }

  for(const data of temp) {
     let closedOneforDay = {
       fitScoreForDay : data.fitScoreForDay,
       UserID : data.userId,
      activitiesForDay : activitiesForDay
     }
     closedOncesForDay.push(closedOneforDay);
 }
})
 return closedOncesForDay;
}
