const moment = require("moment");
const axios = require("axios");
const async = require("async");
const _ = require("lodash")
exports.getUserProfile = async function (req, res, db) {
    const userRef = db.collection('users');
    const calendarRef = db.collection("calendar")
    const activitiesRef = db.collection("activities")

    const userId = req.body.userId;
    let weeklyFitScore 
    let weeklyCaloriesBurned 
    let weeklyTotalNumberOfActivities 
    let weeklyTotalSteps 

    let totalFitScore 
    let totalCaloriesBurned
    let totalNumberOfActivities
}