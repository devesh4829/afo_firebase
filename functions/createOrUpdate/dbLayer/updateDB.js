const moment = require("../../node_modules/moment");
const _ = require("../../node_modules/lodash")
const { DATE_FORMAT,CALENDAR_ID_KEY,ACTIVITY_ID_DATE_TIME_FORMAT, USER_ID_KEY, RAW_DATE_KEY, ACTIVITY_ID_KEY } = require("../../constants");
const { Football } = require("../../activityMappers/activityMasterData");
const { user } = require("../../node_modules/firebase-functions/lib/providers/auth");
const { forEach } = require("../../node_modules/async");
const { deprecationHandler } = require("../../node_modules/moment");
let calendarToSet = []

let calendarIds = []
/**
 * It is responsible for storing data in db 
 * Its a 5 step process:
 * 1. Update all the activities in activity collection
 * 2. getUserActivityDetails(this is done to populate cumulative data at userlevel)
 * 3. prepareCalenderToSet(details: ref comment above method calling)
 * 4. getUserActivityToUpdate (this will subtract duplicate/redundant/false data,
 *  which is already present for calander documents to be update)
 * 5. UpdateCalender data :  update all calendar data in dp
 * 6. Update userObject with updated activityDetailObject calculated in step 4
 * @param {*} db 
 * @param {*} updateCollectionRequest 
 */
const updateDB = async function (db, updateCollectionRequest){

    let userID = updateCollectionRequest.userID
    let isUserExist = updateCollectionRequest.isUserExist
    let primarySourceID = updateCollectionRequest.PrimarySourceID
    let primarySourceName = updateCollectionRequest.PrimarySourceName
    let token = updateCollectionRequest.token
    let totalNumberOfActivities = updateCollectionRequest.totalNumberOfActivities
    let apiActivities = updateCollectionRequest.apiActivities
    console.log("updateCollectionRequest ", updateCollectionRequest)

    let calendarRef = db.collection("calendar")

    // apiActivities.forEach(element => {
    //     console.log("activity_id : ", element.ActivityID)
    //     console.log("activity_name", element.ActivityName)
    //     console.log("activity_duration", element.Duration)
    //  });

    //Update Activity Collection
    await updateActivityCollection(db, apiActivities, userID)

    let userActivityDetails = {
        Steps:0,
        Duration: 0,
        Count: 0,
        Calories : 0,
        FitScore : 0
    }

    if(isUserExist){
     userActivityDetails =  await getUserActivityDetails(db, userID, userActivityDetails)
    }

    console.log("getting user activity details", userActivityDetails)

    
     
    //Prepare data to be updated in Calendar collection
    //This will perform 3 operations:
    //1. calculate activity details(steps, duration, fitscore, calories, activity count) to be updated at calendar level
    //2. Add these values to overall activity details for the user
    //3. getting all the calenderIds and calendarObjects to be updated in db
    userActivityDetails = await prepareCalenderToSet(apiActivities, userID, userActivityDetails) 
    console.log("user activity details after adding the activities", userActivityDetails)

    userActivityDetails = await getUserActivityToUpdate(db, userActivityDetails)

    console.log("user activity details after subtracting the activities", userActivityDetails)
    
    await updateCalendarCollection(db, calendarToSet)

     //Calculate weekly fit score 
     let weeklyCalendarData = await calendarRef.where(RAW_DATE_KEY,">=", moment().startOf('week').valueOf()).where(USER_ID_KEY,"==",userID).get();
     let weeklyFitScore = 0 ;  
     weeklyCalendarData.forEach(doc => {
         weeklyFitScore += Number(doc.data().DailyFitScore) ;
     });

     
    await updateUserCollection(db, userID, primarySourceID, primarySourceName,userActivityDetails, token, weeklyFitScore)
    
}

const updateUserCollection = async function (db, userID, primarySourceID, primarySourceName, userActivityDetails, token, weeklyFitScore){
    console.log("inside updateUserCollection")
    let userRef = db.collection("users");
    try{
          await userRef.doc(userID.toString()).set({
                UserID: userID,
                PrimarySourceID: primarySourceID, 
                PrimarySourceName: primarySourceName,
                LastActivityUpdated: moment().utcOffset("+05:30").valueOf(),
                AccessToken: token,
                WeeklyFitScore: weeklyFitScore ? weeklyFitScore : 0,
                FitScore: userActivityDetails.FitScore ? userActivityDetails.FitScore : 0,
                Steps: userActivityDetails.Steps ? userActivityDetails.Steps : 0,
                Count: userActivityDetails.Count ?userActivityDetails.Count : 0, 
                Calories: userActivityDetails.Calories ? userActivityDetails.Calories : 0,
                Duration: userActivityDetails.Duration ? userActivityDetails.Duration : 0
    });
    } catch (e) {
        console.log("Exception in Updating User Collection", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    callback = {
                data: "Data updated successfully",
                status: 200,
                result: true
            }
            return callback
}

const prepareCalenderToSet = async function (apiActivities, userID, userActivityDetails){
        //prepare update Calendar Collection
    console.log("inside prepare updateCalenderCollection")
    let activityByDate = []
    activityByDate = _.groupBy(apiActivities, function (o) {
        return moment(o.StartDate).utcOffset("+05:30").format(DATE_FORMAT)
    })
    for (let key in activityByDate) {
        let obj = {
            Date : key,                
            UserID: userID
        }
            
        let dailyFitScore = 0
        let dailyCalories = 0
        let dailySteps = 0
        let dailyDuration = 0
        let calendarId = key+"_"+userID
        
        let activityIDs = []
        for (let i=0; i<activityByDate[key].length; i++) {
            
                    
            dailyFitScore += activityByDate[key][i].ActivityScore ? activityByDate[key][i].ActivityScore : 0
            dailyCalories += activityByDate[key][i].CaloriesBurned ? activityByDate[key][i].CaloriesBurned :0
                    
            if(activityByDate[key][i].ActivityName == "Daily Steps"){
                dailySteps = activityByDate[key][i].StepCount
            }
            dailyDuration += activityByDate[key][i].Duration ? activityByDate[key][i].Duration : 0
            activityIDs.push(activityByDate[key][i].ActivityID)
        }
        //Update daily values
        obj.DailyFitScore = dailyFitScore
        obj.ActivityIDs = activityIDs
        obj.DailyActivityCount = activityIDs.length
        obj.DailyCalories = dailyCalories
        obj.DailySteps = dailySteps
        obj.DailyDuration = dailyDuration
        obj.CalendarID = calendarId

        //Update overall user activity details
        userActivityDetails.FitScore += dailyFitScore
        userActivityDetails.Count += activityIDs.length
        userActivityDetails.Steps += dailySteps
        userActivityDetails.Duration += dailyDuration
        userActivityDetails.Calories += dailyCalories


           
        calendarIds.push(calendarId)



        calendarToSet.push(obj)
    }
    return userActivityDetails;
}    

const updateCalendarCollection = async function(db, calendarToSet){
    console.log("inside updateCalenderCollection")
    let calendarRef = db.collection("calendar")
    //Set calendar 
    try{
        for (let i =0;i<calendarToSet.length;i++) {
            await calendarRef.doc(calendarToSet[i].CalendarID).set({
                Date: calendarToSet[i].Date,
                RawDate : moment(calendarToSet[i].Date, DATE_FORMAT).valueOf(),
                UserID: calendarToSet[i].UserID,
                ActivityIDs: calendarToSet[i].ActivityIDs,
                DailyFitScore: calendarToSet[i].DailyFitScore ? calendarToSet[i].DailyFitScore : 0,
                DailyActivityCount: calendarToSet[i].DailyActivityCount ? calendarToSet[i].DailyActivityCount: 0,
                DailyCalories: calendarToSet[i].DailyCalories ? calendarToSet[i].DailyCalories : 0,
                DailySteps: calendarToSet[i].DailySteps ? calendarToSet[i].DailySteps : 0,
                DailyDuration: calendarToSet[i].DailyDuration ? calendarToSet[i].DailyDuration : 0,
                CalendarID : calendarToSet[i].CalendarID

            });            
        }
        
    } catch (e) {
        console.log("Exception in Updating Calendar collection : ", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
}    

    //Update Activity Collection
const updateActivityCollection = async function (db, apiActivities, userID){
   // console.log("inside updateActivityCollection ", apiActivities)
    console.log("inside updateActivityCollection userID ", userID)
    let activitiesRef = db.collection("activities")
    try{  for (let i=0;i<apiActivities.length;i++) {
                
        await activitiesRef.doc(apiActivities[i].ActivityID).set({
            UserID: userID, 
            ActivityName: apiActivities[i].ActivityName, 
            ActivityID: apiActivities[i].ActivityID, 
            StartDate: apiActivities[i].StartDate ? apiActivities[i].StartDate : undefined,
            EndDate: apiActivities[i].EndDate ? apiActivities[i].EndDate : undefined,
            Duration: apiActivities[i].Duration ? apiActivities[i].Duration : 0,
            CaloriesBurned: apiActivities[i].CaloriesBurned ? Math.round(Number(apiActivities[i].CaloriesBurned)) : 0,
            ActivityScore: apiActivities[i].ActivityScore ? Math.round(Number(apiActivities[i].ActivityScore)) : 0,
            StepCount: apiActivities[i].StepCount ? apiActivities[i].StepCount: 0,
            Distance: apiActivities[i].Distance ? Math.round(Number(apiActivities[i].Distance)) : 0,
            AveragePace: apiActivities[i].AveragePace ? Math.round(Number(apiActivities[i].AveragePace)) : 0,
            Source : apiActivities[i].Source
            });
        }
    } catch (e) {
        console.log("Exception in updating Activities", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    }

// this will subtract duplicate/redundant/false data which is already present for calander documents to be update)
const getUserActivityToUpdate = async function(db, userActivityDetails){
    console.log("inside getUpdatedActivityDetails")
    let calendarRef = db.collection("calendar") 
    try{
        if(calendarIds != null && calendarIds.length > 0) {
            
            let i,j,calendarIdsChunk,chunk = 10;
            for (i=0, j=calendarIds.length; i<j; i+=chunk) {
                calendarIdsChunk = calendarIds.slice(i, i+chunk)
                console.log("Calendar IDs :", calendarIdsChunk)
                let calendarsToUpdate = await calendarRef.where(CALENDAR_ID_KEY, "in", calendarIdsChunk).get()
                let someNumber=0;
                calendarsToUpdate.forEach(doc => {
                    someNumber++
                    userActivityDetails.FitScore -= doc.data().DailyFitScore ? Number(doc.data().DailyFitScore) : 0
                    userActivityDetails.Steps -= doc.data().DailySteps ? Number(doc.data().DailySteps) : 0
                    userActivityDetails.Count -= doc.data().DailyActivityCount? Number(doc.data().DailyActivityCount) : 0
                    userActivityDetails.Calories -= doc.data().DailyCalories ? Number(doc.data().DailyCalories) : 0
                    userActivityDetails.Duration -= doc.data().DailyDuration ? Number(doc.data().DailyDuration) : 0
                });

                console.log("Cal from db :", someNumber)
                calendarIds = []
            }
        }
    } catch(e) {
        console.log("Exception in Updating Calendar collection in : getUpdatedActivityDetails", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }

    return userActivityDetails
} 

const getUserActivityDetails = async function(db, userID, userActivityDetails){
    console.log("inside getUserActivityDetails")
    let userRef = db.collection("users");
    let dbUserActivityDetails = await userRef.where(USER_ID_KEY, "==", userID).get()
    //console.log("dbUserActivityDetails.doc.data()", dbUserActivityDetails)
     dbUserActivityDetails.forEach(doc=>{ 
        userActivityDetails.FitScore = doc.data().FitScore ? Number(doc.data().FitScore) : 0
        userActivityDetails.Steps = doc.data().Steps ? Number(doc.data().Steps) : 0
        userActivityDetails.Count = doc.data().Count ? Number(doc.data().Count) : 0
        userActivityDetails.Calories = doc.data().Calories ? Number(doc.data().Calories) : 0
        userActivityDetails.Duration = doc.data().Duration ? Number(doc.data().Duration) : 0
    })

    return userActivityDetails
}

module.exports = { updateDB }