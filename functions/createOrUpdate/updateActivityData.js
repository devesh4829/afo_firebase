const moment = require("../node_modules/moment");
const _ = require("../node_modules/lodash")
const { DATE_KEY, DATE_FORMAT,ACTIVITY_ID_KEY,ACTIVITY_ID_DATE_TIME_FORMAT } = require("../constants");
const googleFitMapping = require("../activityMappers/googleFitMapping");
const activityMasterData = require("../activityMappers/activityMasterData");
const getDataFromFitBit = require("./fitBit/getDataFromFitbit")
const getDataFromGoogleFit = require("./googleFit/getDataFromGoogleFit");
const updateDB = require("./dbLayer/updateDB");
const garminAPI = require('./garminConnect/garminDataRetrieval');


exports.updateActivityData = async function (req, res, db) {

    console.log("Request",req.body)
    let userID = req.body.userID 
    let token = req.headers.authorization
    let PrimarySourceID = req.body.sourceID || "1"
    let PrimarySourceName = req.body.primarySourceName || "GoogleFit"
    let totalNumberOfActivities = 0;
   // const endDate = req.body.endDate
   // endDate = currentDate


    console.log("token:  ", token)
    let apiEndDate = moment().utcOffset("+05:30")
    let apiStartDate;
    let apiActivities = []
    let callback = {}
    let updateAPIRequest = {}
    let updateDBRequest = {}
    let activitiesRef = db.collection("activities")

    console.log("EndDate :", apiEndDate)
    
    try {
        let userRef = db.collection("users");
        let userDetails = await userRef.where("UserID", "==", userID).get()
        let lastActivityUpdated 
        let isUserExist = !userDetails.empty
        
        if (!isUserExist) {
            //No data is present for the user
            //Pull data from start of week before requested date
            apiStartDate =  moment().utcOffset("+05:30").subtract(7,"day").startOf('week');
        } else {
            //Data is present for the user
            userDetails.forEach(doc => {
                lastActivityUpdated = moment(doc.data().LastActivityUpdated).utcOffset("+05:30")
                totalNumberOfActivities = doc.data().TotalNumberOfActivities
            });
            //Get data from one day before lastActivityUpdated
             apiStartDate = moment(lastActivityUpdated).utcOffset("+05:30").subtract(1,"day").startOf('day')
             console.log("Api Start Date", apiStartDate)
        }

        console.log("Api Start Date", apiStartDate)

        updateAPIRequest = {
            token, apiStartDate, apiEndDate, userID
        }

        if (PrimarySourceID == 1) {
            try{
          let   updateAPIResponse = await getDataFromGoogleFit.getGoogleFitData(updateAPIRequest)
          apiActivities = updateAPIResponse.apiActivities
            } catch(e) {
                callback = {
                    data : e,
                    status: 402,
                    result: false,
                    message : "Error in fetching google dataa"
                } 
                return callback 
            }
        } else if (PrimarySourceID == 2) {
           try {
                updateAPIRequest.sourceUserId = req.body.sourceUserId
                updateAPIRequest.userDetails = userDetails
                let updateAPIResponse = await getDataFromFitBit.getDataFromFitbit(updateAPIRequest)
                apiActivities = updateAPIResponse.apiActivities
           } catch(e) {
            callback = {
                data : e,
                status: 402,
                result: false,
                message : "Error in fetching fitbit dataa"
            }  
            return callback
           }
        } else if (primarySourceID === 4){
            try{
                userTokenData = await garminAPI.updateGarminData();
                return userTokenData;
            }catch(e){
                callback = {
                    data : e,
                    status: 402,
                    result: false,
                    message : "error calling garmin api"
                }  
                return callback
            }
        }
        
        updateDBRequest ={
            userID, token, PrimarySourceID, PrimarySourceName, totalNumberOfActivities, apiActivities, isUserExist
        }
        
        //Update Collection
        await updateDB.updateDB(db, updateDBRequest);
        
        callback = {
            data: "Data updated successfully",
            status: 200,
            result: true
        }

        } catch (e) {
        console.log("Exception 2 ", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
        return callback   
    };
  return callback
}