const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const admin = require('firebase-admin');
const _ = require("lodash");
const { USER_ID_KEY, PRIMARY_SOURCE_ID } = require('../constants');

exports.deleteUserData = async function(req, res, db) {
    let userID = req.body.userID
    let userIdGroup =[]
    userIdGroup = req.body.userIDs
    let primarySourceID = req.body.primarySourceID
    const activitiesRef = db.collection("activities")
    const calendarRef = db.collection("calendar")
    const userRef = db.collection("users")
    let userIDs = [];

    let usersDeleted = 0;
    let calendarsDeleted = 0;
    let activitiesDeleted = 0;
    
    if(userID) {
        userIDs.push(userID)
    } 
    if (primarySourceID) {
       let userRes = await userRef.where(PRIMARY_SOURCE_ID,'==',primarySourceID).get();
       userRes,array.forEach(doc => {
           userIDs.push(doc.data().UserID)
       });
    }
    if (userIdGroup && userIdGroup.length>= 1) {
      userIdGroup.array.forEach(element => {
        userIDs.push(element)
      });
    }

    if(userIDs.length<1){
      return {
          message : "No User ID or primary source Provided"
      }
    }
    // delete user
    try {
      console.log("UserIDs", userIDs)
        let userData = await getDataFromDbWithUserIds(userRef, userIDs)
        userData.forEach((doc) => {
          doc.ref.delete();
          usersDeleted++
        });
    } catch (error) {
        return {
          status: 'error', msg: 'Error while deleting User ', data: error,
        }
    }
     //delete calanders
    try {
        let calendarData = await getDataFromDbWithUserIds(calendarRef, userIDs)
        calendarData.forEach((doc) => {
          doc.ref.delete();
          calendarsDeleted++
        });
    } catch (error) {
      return {
      status: 'error', msg: 'Error while deleting Calendar', data: error,
      }
    }
    //delete activities
    try {
      let activitiesData = await getDataFromDbWithUserIds(activitiesRef, userIDs)
      activitiesData.forEach((doc) => {
        doc.ref.delete();
        activitiesDeleted++
      });
    } catch (error) {
      return {
        status: 'error', msg: 'Error while deleting Activities', data: error,
      }
    }

    return {
      NumberOfActivitiesDeleted : activitiesDeleted,
      NumberOfCalendarsDeleted : calendarsDeleted,
      NumberOfUsersDeleted : usersDeleted
    }
}

let getDataFromDbWithUserIds = async function(collectionRef,userIds) {
  var i,j,userIdsChunk,chunk = 10;
  let documents = []

  //This needs to be done as firestore does not support more than element filtering with "in" query
 try{
      for (i=0,j=userIds.length; i<j; i+=chunk) {

          userIdsChunk = userIds.slice(i,i+chunk);

          let document = await collectionRef.where(USER_ID_KEY, "in", userIdsChunk).get()
          console.log(document)
          document.forEach(element => {
              documents.push(element)
          });
      }

      return documents;
  } catch(e) {
    console.log(e)
    throw e
  }
}